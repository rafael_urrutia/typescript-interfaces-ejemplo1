function saludo(person) {
    return "Hola, " + person.firstName + " " + person.lastName;
}
var user = { firstName: "Rafael", lastName: "Urrutia" };
document.body.innerHTML = saludo(user);
