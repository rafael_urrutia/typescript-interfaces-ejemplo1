interface Person {
    firstName: string;
    lastName: string;
    age?: number;
}

function saludo(person: Person) {
    return "Hola, " + person.firstName + " " + person.lastName;
}

let user = { firstName: "Rafael", lastName: "Urrutia" };

document.body.innerHTML = saludo(user);
